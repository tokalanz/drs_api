import unittest
import os

os.environ['FLASK_SERVER_NAME'] = '0.0.0.0'
os.environ['FLASK_SERVER_PORT'] = '8080'
# This script requires a memcached instance to connect to.
os.environ['MEMCACHED_NAME'] = '10.0.8.50:11211'

from modules.mymemcache import shared
from modules.restplus import api, flask_app
from maint.maintenance import maint_api
import memcache
import settings



class BasicTests(unittest.TestCase):
    def setUp(self):
        flask_app.config['TESTING'] = True
        flask_app.config['DEBUG'] = True
        self.app = flask_app.test_client()
        
    def tearDown(self):
        os.environ['FLASK_SERVER_NAME'] = ''
        os.environ['FLASK_SERVER_PORT'] = ''
        os.environ['MEMCACHED_NAME'] = ''        
        pass
    
    def get_status(self):
        return self.app.get('/maintenance', follow_redirects=True)
    
    def add_node(self, node):
        return self.app.put('/maintenance/' + node, follow_redirects=True)
    
    def del_node(self, node):
        return self.app.delete('/maintenance/' + node, follow_redirects=True)
    
    def test_status_page(self):
        response = self.get_status()
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'status', response.data)
        
    def test_swagger_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'swagger', response.data)
        
    def test_add_page(self):
        response = self.add_node('testing')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'testing', response.data)
        
    def test_del_page(self):
        response = self.del_node('testing')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'testing', response.data)
        
if __name__ == "__main__":
    unittest.main()
    