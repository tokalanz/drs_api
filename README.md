# Proxmox DRS API

The functionality of this API is limited at the moment. It's only current function is to notify Proxmox DRS to put a Proxmox node into Maintenance mode.

Maintenance mode will trigger DRS to live migrate all virtual machine off the selected node and leave the node empty until it is removed from the API queue.


## Requirements:

The Proxmox DRS API runs inside a Docker container and also required an instance of memcached accessible by a network connection.

All packages required for the Proxmox DRS API are installed into the container at build time.



## Installation:

Use Git to clone the repository to your Docker host or Docker build server.

> git clone https://gitlab.com/tokalanz/drs_api.git

Change to the DRS API directory and edit the Dockerfile with the memcached IP address.

> cd drs_api

> nano Dockerfile

Edit the MEMCACHED_NAME environment variable to point to your memcached instance.


Build the container

> docker build -t drs_api .


## Running the container:

If you do not have a memcached instance available you can use a Docker container

> docker run -p 11211:11211 -d memcached:latest

Run the DRS API container

> docker run -p 8080:8080 -d drs_pve:latest


## Testing:

To test the API is working and to view the endpoints simply browse to the API URL (change localhost to your Docker host IP address)

> Browse to http://localhost:8080

The SwaggerUI will show the methods and calls to use. You can also use the SwaggerUI to test adding nodes to the queue and removing them.



