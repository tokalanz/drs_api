import os

# Flask Settings
FLASK_SERVER_NAME = os.environ['FLASK_SERVER_NAME']
FLASK_SERVER_PORT = os.environ['FLASK_SERVER_PORT']
FLASK_DEBUG = True

# Flask-Restplus Settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# memcached Settings
MEMCACHED_NAME = os.environ['MEMCACHED_NAME']
