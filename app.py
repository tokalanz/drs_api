from flask import Flask, jsonify
from flask_restplus import Api, Namespace, Resource
from modules.mymemcache import shared
from modules.restplus import api, flask_app
from maint.maintenance import maint_api
import memcache
import settings


if __name__ == '__main__':
    flask_app.run(debug=settings.FLASK_DEBUG, host=settings.FLASK_SERVER_NAME, port=settings.FLASK_SERVER_PORT)
    