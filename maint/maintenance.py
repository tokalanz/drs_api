from flask import Flask, jsonify
from flask_restplus import Api, Namespace, Resource
from modules.restplus import api, flask_app
from modules.mymemcache import shared

maint_api = Namespace('maintenance')
api.add_namespace(maint_api)

@maint_api.route('/')
class Maint_Status(Resource):
    def get(self):
        """
        Returns any node queued for maintenance mode and status
        
        Use this method to get the current status of any nodes in the queue.
        
        
        ```
        Status:
                waiting - node is loaded into the queue but not yet seen by DRS
                processing - DRS is in the process of evacutating the VMs off the node
                complete - DRS has finished evacuating the VMs off the node
        ```
        """

        try:
            myNode = shared.get('node')
            in_maint = shared.get('in_maint')
            myStatus = shared.get('status')
            response = jsonify(action='status', node=myNode, in_maint=in_maint, status=myStatus)
            response.status_code = 200
            return response
        except Exception as e:
            response = jsonify(action='status', node='', in_maint='', status='not found')
            response.status_code = 404
            return response
        
@maint_api.route('/<string:node>')
class Maint_Add(Resource):
    @api.response(200, 'Node added to queue')
    def put(self, node):
        """
        Add {node} to the queue for maintenance mode
        """
        try:
            shared.set('node', node)
            shared.set('in_maint', 0)
            shared.set('status', 'waiting')
            response = jsonify(action='add', node=node, in_maint=0, status='waiting')
            response.status_code = 200
            return response
        except Exception as e:
            response = jsonify(action='add error', node=e, in_maint='', status='error')
            response.status_code = 500
            return response            
        
    @api.response(200, 'Node deleted from queue')
    def delete(self, node):
        """
        Remove {node} from maintenance mode queue
        """
        try:
            myNode = shared.get('node')
            in_maint = shared.get('in_maint')
            myStatus = shared.get('status')
            if myNode == node:
                shared.delete('node')
                shared.delete('in_maint')
                shared.delete('status')
                response = jsonify(action='del', node=node, in_maint=in_maint, status=myStatus)
                response.status_code = 200
                return response        
            else:
                response = jsonify(action='not found', node=node, in_maint=0, status='')
                response.status_code = 404
                return response   
        except Exception as e:
            response = jsonify(action='del error', node=e, in_maint='', status='error')
            response.status_code = 500
            return response  
