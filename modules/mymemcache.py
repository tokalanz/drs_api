import memcache
import settings

# Connect to the memcached server listed in settings
try:
    print('memcached ' + settings.MEMCACHED_NAME)
    shared = memcache.Client([settings.MEMCACHED_NAME], debug=1)
except Exception as e:
    message = 'Unable to connect to memcached on ' + settings.MEMCACHED_NAME
    exit(1)
    
