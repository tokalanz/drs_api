from flask import Flask
from flask_restplus import Api, Namespace, Resource
import settings

flask_app = Flask(__name__)
#api = Api(flask_app, title='PVE DRS API', description='API to control DRS functions')

api = Api(flask_app, version='1.0', title='PVE DRS API', description='API to control DRS maintenance functions')

# caption unhandled exceptions
@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled error occured.'
    if not settings.FLASK_DEBUG:
        return {'message': message}, 500
    