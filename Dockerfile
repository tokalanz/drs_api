FROM python:3.5

COPY requirements.txt /root/

RUN apt-get update && \
    pip install -r /root/requirements.txt && \
    rm -r /root/.cache

WORKDIR /root/

EXPOSE 8080

COPY *.py /root/
COPY modules/* /root/modules/
COPY maint/* /root/maint/

ENV FLASK_ENV=development
ENV FLASK_APP=app.py
ENV MEMCACHED_NAME='memcached:11211'
ENV FLASK_SERVER_NAME='0.0.0.0'
ENV FLASK_SERVER_PORT='8080'

CMD [ "python", "app.py" ]
